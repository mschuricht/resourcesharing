import * as cdk from '@aws-cdk/core';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as ram from '@aws-cdk/aws-ram';
import * as iam from '@aws-cdk/aws-iam';
import * as s3 from '@aws-cdk/aws-s3';



export class CrossAccountRoleStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);
        const acctParam = new cdk.CfnParameter(this, "SharingAccountID", {
            type: "String",
            description: 'The account which will be allowed to assume the role',
            maxLength: 12,
            minLength: 12,
            allowedPattern: '[0-9]{12}'
        })

        const role = new iam.Role(this, 'CrossAccountS3Access', {
            roleName: 'CrossAccountS3Access',
            managedPolicies: [iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonS3FullAccess')],
            assumedBy: new iam.AccountPrincipal(acctParam.valueAsString)
        })

        const bucket = new s3.Bucket(this, "demo-bucket", {
          bucketName: cdk.PhysicalName.GENERATE_IF_NEEDED,
          accessControl: s3.BucketAccessControl.PRIVATE,
          encryption: s3.BucketEncryption.S3_MANAGED,
          versioned: true,
          blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL
        })

        new cdk.CfnOutput(this, "CrossAccountRole", {
          value: role.roleName,
          exportName: "CrossAccountRole"
        })

        new cdk.CfnOutput(this, "S3Bucket", {
          value: bucket.bucketName,
          exportName: "S3Bucket"
        })
    }
}
