import * as cdk from '@aws-cdk/core';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as ram from '@aws-cdk/aws-ram';
import * as iam from '@aws-cdk/aws-iam';
import * as s3 from '@aws-cdk/aws-s3';


export class BaseVpcStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const acctParam = new cdk.CfnParameter(this, "SharedAccountID", {
            type: "String",
            maxLength: 12,
            minLength: 12,
            allowedPattern: '[0-9]{12}',
            description: 'The account which will be allowed to share the resources',
          })
          const vpc = new ec2.Vpc(this, 'Base VPC', {
              cidr: '10.0.0.0/16',
              subnetConfiguration: [
                  { subnetType: ec2.SubnetType.PUBLIC, cidrMask: 24, name: 'Web Subnet '},
                  { subnetType: ec2.SubnetType.ISOLATED, cidrMask: 24, name: 'Application Subnet '},
                  { subnetType: ec2.SubnetType.ISOLATED, cidrMask: 24, name: 'Database Subnet '},
              ],
              maxAzs: 1
          })
  
          const subnetArns = vpc.publicSubnets.map(s => `arn:aws:ec2:${this.region}:${this.account}:subnet/${s.subnetId}`)
          new ram.CfnResourceShare(vpc, 'Shared Web Subnets', {
              allowExternalPrincipals: false,
              name: 'Shared Web Subnets',
              principals: [acctParam.valueAsString],
              resourceArns: [...subnetArns]
          })
  
          new cdk.CfnOutput(this, "VPCInfo", {
            value: vpc.vpcCidrBlock,
            exportName: "VPCInfo"
          })
    }
}